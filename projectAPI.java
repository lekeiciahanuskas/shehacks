

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import java.io.IOException;

OkHttpClient httpClient = new OkHttpClient();

String apiAddress = "https://api.example.com/data"; // Replace with your API address

Request request = new Request.Builder()
    .url(apiAddress)
    .addHeader("Authorization", "Bearer YOUR_ACCESS_TOKEN") // Add headers if required
    .build();