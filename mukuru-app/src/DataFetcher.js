import React, { Component } from 'react';
import APIToReact from './apiFetch.mjs'; // Assuming this is the file containing your APIToReact class

class DataFetcher extends Component {
  state = {
    data: null,
    error: null
  };

  componentDidMount() {
    const apiInstance = new APIToReact();
    const url = 'https://api-uct.mukuru.com/taurus/v1/products/price-check';

    apiInstance
      .fetchData(url)
      .then(data => {
        this.setState({ data });
      })
      .catch(error => {
        this.setState({ error });
      });
  }

  render() {
    const { data, error } = this.state;

    if (error) {
      return <div>Error: {error.message}</div>;
    }

    if (!data) {
      return <div>Loading...</div>;
    }

    // Render the fetched data here
    return (
      <div>
        <h1>Fetched Data</h1>
        <pre>{JSON.stringify(data, null, 2)}</pre>
      </div>
    );
  }
}

export default DataFetcher;
