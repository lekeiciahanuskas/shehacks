
const API_BASE_URL = 'https://api-uct.mukuru.com/taurus/v1';

export const fetchData = async (url) => {
  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error('Fetch error: ' + error.message);
  }
}

export const fetchCountryData = async () => {
  try {
    const response = await fetchData(`${API_BASE_URL}/resources/countries`);
    return response.data.items;
  } catch (error) {
    throw new Error('Failed to fetch country data.');
  }
};

export const fetchProductData = async () => {
  try {
    const response = await fetchData(`${API_BASE_URL}/resources/product-types`);
    return response.data.items;
  } catch (error) {
    throw new Error('Failed to fetch product data.');
  }
};

export const fetchCurrencyData = async () => {
  try {
    const response = await fetchData(`${API_BASE_URL}/resources/currencies`);
    return response.data.items;
  } catch (error) {
    throw new Error('Failed to fetch currency data.');
  }
};

export const fetchEveryThing = async () => {
  try {
    const response = await fetchData(`${API_BASE_URL}/products/price-check`);
    return response.data.items;
  } catch (error) {
    throw new Error('Failed to fetch product data.');
  }
};
