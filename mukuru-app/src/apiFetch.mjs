import fetch from 'node-fetch';
class APIToReact {
    async fetchData(url) {
        try {
      const response = await fetch(url);
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
      const data = await response.json();
            return data;
        } catch (error) {
            throw new Error('Fetch error: ' + error.message);
        }
    }
    }

    const apiInstance = new APIToReact();
    const url = 'https://api-uct.mukuru.com/taurus/v1/products/price-check'
    apiInstance.fetchData(url)
      .then(data => {
        console.log(data);
      })
      .catch(error => {
        console.error(error);
      });

