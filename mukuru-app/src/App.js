import React, { useState, useEffect } from 'react';
import './App.css';
import axios from 'axios';

function App() {
  const [countries, setCountries] = useState([]);
  const [payInCountry, setPayInCountry] = useState('');
  const [payOutCountry, setPayOutCountry] = useState('');
  const [payInAmount, setPayInAmount] = useState('');
  const [cost, setCost] = useState('');

  useEffect(() => {
  async function fetchCountries() {
    try {
      const response = await axios.get(
        'https://api-uct.mukuru.com/taurus/v1/resources/countries'
      );
      console.log("data is supposed to show up here: "+response.data.items);

      setCountries(response.data.items);
    } catch (error) {
      console.error('Error fetching countries:', error);
    }
  }

  fetchCountries();
}, []);


  const handleCalculate = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.get(
        'https://api-uct.mukuru.com/taurus/v1/products/price-check',
        {
          params: {
            pay_in_country: payInCountry,
            pay_out_country: payOutCountry,
            pay_in_amount: payInAmount,
          },
        }
      );

      setCost(response.data.pay_out_amount);
    } catch (error) {
      console.error('Error calculating cost:', error);
    }
  };

  return (
    <div className="App">
      <h1>Remittance Calculator</h1>
      <form onSubmit={handleCalculate}>
        <label htmlFor="payInCountry">Sending From:</label>
        <select
          id="payInCountry"
          value={payInCountry}
          onChange={(e) => setPayInCountry(e.target.value)}
          required
        >
          <option value="">Select Country</option>
          {countries.map((country) => (
            <option key={country.code} value={country.code}>
              {country.name}
            </option>
          ))}
        </select>

        <label htmlFor="payOutCountry">Receiving In:</label>
        <select
          id="payOutCountry"
          value={payOutCountry}
          onChange={(e) => setPayOutCountry(e.target.value)}
          required
        >
          <option value="">Select Country</option>
          {countries.map((country) => (
            <option key={country.code} value={country.code}>
              {country.name}
            </option>
          ))}
        </select>

        <label htmlFor="payInAmount">Amount:</label>
        <input
          type="number"
          id="payInAmount"
          value={payInAmount}
          onChange={(e) => setPayInAmount(e.target.value)}
          step="0.01"
          required
        />

        <button type="submit">Calculate</button>
      </form>

      {cost && (
        <div>
          <h2>Calculated Cost</h2>
          <p>{cost}</p>
        </div>
      )}
    </div>
  );
}

export default App;
