from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
import httpx
from fastapi.middleware.cors import CORSMiddleware


app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)
async def proxy_api():
    api_url = "https://api-uct.mukuru.com/taurus/v1/products/price-check"
    
    async with httpx.AsyncClient() as client:
        response = await client.get(api_url)
        return response.json()

@app.get("/calculate-remittance/")
async def calculate_remittance(pay_in_country: str, pay_out_country: str, amount: float, currency: str="receivers"):
    json_data = await proxy_api()

    for item in json_data.get("items", []):
        if item.get("calculatorType") == "you-pay" and item.get("payInCountryCode") == pay_in_country and item.get("payOutCountryCode") == pay_out_country:
            fee = item.get("fee", {}).get("amount", 0)
            rate = item.get("rate", {}).get("rate", 1)
            total_cost = amount + fee
            converted_amount = amount * rate
            
            if currency == "senders":
                converted_amount = converted_amount / rate

            return {
                "pay_in_country": pay_in_country,
                "pay_out_country": pay_out_country,
                "amount": amount,
                "conversion_rate": rate,
                "fee": fee,
                "total_cost": total_cost,
                "converted_amount": converted_amount
            }
    
    raise HTTPException(status_code=404, detail="No data available for the provided countries.")

@app.get("/countries/")
async def get_countries():
    json_data = await proxy_api()
    
    countries = set()
    
    for item in json_data.get("items", []):
        pay_in_country = item.get("payInCountryCode")
        pay_out_country = item.get("payOutCountryCode")
        
        if pay_in_country:
            countries.add(pay_in_country)
        if pay_out_country:
            countries.add(pay_out_country)
    
    return {"countries": list(countries)}

# Custom exception handler
@app.exception_handler(HTTPException)
async def http_exception_handler(request, exc):
    return JSONResponse(
        status_code=exc.status_code,
        content={"detail": exc.detail},
    )
