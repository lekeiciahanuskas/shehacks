document.addEventListener("DOMContentLoaded", async () => {
    const youSendCountryCodeSelect = document.getElementById("youSendCountryCode");
    const recipientGetsCountryCodeSelect = document.getElementById("recipientGetsCountryCode");
    const amountInput = document.getElementById("amount");
    const currencyDisplaySelect = document.getElementById("currencyDisplay");
    const calculateButton = document.getElementById("calculateButton");
    const responseContainer = document.getElementById("responseContainer");
    const feeContainer = document.getElementById("feeContainer"); // New container
    const totalCostContainer = document.getElementById("totalCostContainer"); // New container
    const convertedAmountContainer = document.getElementById("convertedAmountContainer"); // New container
    
    try {
        const response = await fetch("http://localhost:8000/countries");
        const data = await response.json();
        
        // Display the fetched countries in both select elements
        youSendCountryCodeSelect.innerHTML = "";
        recipientGetsCountryCodeSelect.innerHTML = "";
        
        data.countries.forEach(countryCode => {
            const option = document.createElement("option");
            option.value = countryCode;
            option.textContent = countryCode;
            youSendCountryCodeSelect.appendChild(option);
            recipientGetsCountryCodeSelect.appendChild(option.cloneNode(true));
        });
        
        // Set the default selections
        youSendCountryCodeSelect.value = "ZA";
        recipientGetsCountryCodeSelect.value = "ZW";
        
    } catch (error) {
        console.error("Error fetching data:", error);
    }
    
    const updateCurrencyDisplay = () => {
        const selectedCurrencyDisplay = currencyDisplaySelect.value;
        const selectedCountryCode = selectedCurrencyDisplay === "receivers"
            ? recipientGetsCountryCodeSelect.value
            : youSendCountryCodeSelect.value;
        
        // You can now use the selectedCountryCode as needed
        // Do something with the value, but don't display it
    };
    
    youSendCountryCodeSelect.addEventListener("change", () => {
        updateCurrencyDisplay();
    });
    
    recipientGetsCountryCodeSelect.addEventListener("change", () => {
        updateCurrencyDisplay();
    });

    amountInput.addEventListener("input", () => {
        amountInput.value = amountInput.value.replace(/\D/g, '');
        updateCurrencyDisplay();
    });
    
    currencyDisplaySelect.addEventListener("change", () => {
        updateCurrencyDisplay();
    });
    
    calculateButton.addEventListener("click", async () => {
        const selectedPayInCountry = youSendCountryCodeSelect.value;
        const selectedPayOutCountry = recipientGetsCountryCodeSelect.value;
        const selectedAmount = amountInput.value;
        const selectedCurrencyDisplay = currencyDisplaySelect.value;
        
        if (selectedAmount === "" || isNaN(selectedAmount) || +selectedAmount <= 0) {
            // Display an error message to the user
            amountInput.placeholder = "Enter amount";

            // Add the input-error class to highlight the input box
            amountInput.classList.add("input-error");
            return;
        }
        
        // Remove the input-error class if the input is valid
        amountInput.classList.remove("input-error");
        
        const apiUrl = `http://localhost:8000/calculate-remittance/?pay_in_country=${selectedPayInCountry}&pay_out_country=${selectedPayOutCountry}&amount=${selectedAmount}&currency=${selectedCurrencyDisplay}`;
        
        try {
            const response = await fetch(apiUrl);
            const calculationData = await response.json();
            
            // Display fee, total cost, and converted amount
            feeContainer.innerText = `Fee: ${calculationData.fee}`;
            totalCostContainer.innerText = `Total Cost: ${calculationData.total_cost}`;
            convertedAmountContainer.innerText = `Converted Amount: ${calculationData.converted_amount}`;
            
            // Clear any previous error messages
            responseContainer.innerText = "";
        } catch (error) {
            console.error("Error calculating remittance:", error);
        }
    });
});
